package com.erhui.springcloud.controller;

import com.erhui.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud.config
 * @ClassName: DeptConsumerController
 * @Description:
 * @date 2020/3/13 14:38
 */
@RestController
public class DeptConsumerController {

    //理解:消费者,不应该有service层
    //RestTemplate...供我们直接调用就可以了!注册到spring中
    //(url,实体:Map,Class<T> responseType)

    //提供多种便捷访问远程http服务的方法,简单的Restful服务模板,由spring提供
    @Autowired
    private RestTemplate restTemplate;

    //provider的url前缀
    //private static final String REST_URL_PREFIX="http://localhost:8001";

    //Ribbon. 我们这里的地址,应该是一个变量,通过服务名来访问
    //这个变量,应该是provider的yml中spring配置的name:springcloud-provider-dept
    private static final String REST_URL_PREFIX="http://SPRINGCLOUD-PROVIDER-DEPT";

    @RequestMapping("/consumer/dept/add")
    public Boolean add(Dept dept){
        //参数:请求的url,请求的数据,响应的类型
        return restTemplate.postForObject(REST_URL_PREFIX+"/dept/add",dept,Boolean.class);
    }


    @RequestMapping("/consumer/dept/get/{id}")
    public Dept get(@PathVariable("id")Long id){
        //选用get还是post,看服务端的配置
        //参数:请求的url,响应的类型
        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/get/"+id,Dept.class);
    }


    @RequestMapping("/consumer/dept/getAll")
    public List<Dept> getAll(){
        return restTemplate.getForObject(REST_URL_PREFIX+"/dept/list",List.class);
    }

}
