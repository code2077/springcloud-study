package com.erhui.springcloud;

import com.erhui.myrule.ErhuiRole;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: DeptConsumer_80
 * @Description:
 * @date 2020/3/13 17:27
 */

//ribbon和eureka整合之后,客户端可以直接调用,不用关心ip地址和端口号

@SpringBootApplication
@EnableEurekaClient //开启Eureka客户端注解
@RibbonClient(name = "SPRINGCLOUD-PROVIDER-DEPT",configuration = ErhuiRole.class)//在微服务启动的时候就去加载我们自定义的ribbon类
public class DeptConsumer_80 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumer_80.class,args);
    }
}