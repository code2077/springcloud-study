package com.erhui.myrule;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author 二灰
 * @PackageName: com.erhui.myrule
 * @ClassName: RandomRule
 * @Description: 自定义一个ribbon的负载均衡算法
 * @date 2020/3/16 12:02
 */
public class ErhuiRandomRule extends AbstractLoadBalancerRule {


    //自定义算法:每个服务,访问5次,换下一个服务(总共3个服务)
    //total=0,默认0,如果total=5,我们指向下一个服务节点
    //index=0,默认0,如果total=5,index+1

    private int total = 0;//被调用的次数
    private int currentIndex = 0; //当前是谁在提供服务


    //@edu.umd.cs.findbugs.annotations.SuppressWarnings(value = "RCN_REDUNDANT_NULLCHECK_OF_NULL_VALUE")
    public Server choose(ILoadBalancer lb, Object key) {
        if (lb == null) {
            return null;
        }
        Server server = null;

        while (server == null) {
            if (Thread.interrupted()) {
                return null;
            }
            List<Server> upList = lb.getReachableServers(); //获得还活着的服务
            List<Server> allList = lb.getAllServers(); //获取所有的服务

            int serverCount = allList.size();
            if (serverCount == 0) {//没有服务直接返回null
                return null;
            }

            // int index = chooseRandomInt(serverCount);//生成区间随机数
            // server = upList.get(index);//从活着的服务中随机获取一个
            //===============================================


            //重写自己的自定义策略
            if (total < 5) {//被调用的次数小于5
                //从活着的服务中,调取当前服务
                server = upList.get(currentIndex);
                total++;
            } else {//被调用次数大于5,当前线程序号+1
                total = 0;
                currentIndex++;
                if (currentIndex >= upList.size()) {
                    //如果当前线程的序号大于 活着服务的数量
                    currentIndex = 0;
                }
                server = upList.get(currentIndex);
            }


            //===========================================
            if (server == null) {

                Thread.yield();
                continue;
            }

            if (server.isAlive()) {
                return (server);
            }

            server = null;
            Thread.yield();
        }

        return server;

    }

    protected int chooseRandomInt(int serverCount) {
        return ThreadLocalRandom.current().nextInt(serverCount);
    }

    @Override
    public Server choose(Object key) {
        return choose(getLoadBalancer(), key);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig clientConfig) {

    }
}
