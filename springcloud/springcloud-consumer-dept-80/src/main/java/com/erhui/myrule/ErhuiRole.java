package com.erhui.myrule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 二灰
 * @PackageName: com.erhui.myrule
 * @ClassName: ErhuiRole
 * @Description: ribbon的配置
 * @date 2020/3/15 22:43
 */
@Configuration
public class ErhuiRole {

    // @Bean
    // public IRule myRole(){
    //     return new RandomRule();
    // }

    //自定义我们自己写的算法
    //自定义算法:每个服务,访问5次,换下一个服务(总共3个服务)
    @Bean
    public IRule myRole(){
        return new ErhuiRandomRule();
    }
}
