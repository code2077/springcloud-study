package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: ZuulApplication_9527
 * @Description:
 * @date 2020/3/17 23:35
 */
@SpringBootApplication
@EnableZuulProxy        //开启zuul这个功能
public class ZuulApplication_9527 {
    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication_9527.class,args);
    }
}
