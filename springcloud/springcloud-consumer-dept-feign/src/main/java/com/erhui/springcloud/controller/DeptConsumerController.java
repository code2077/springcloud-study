package com.erhui.springcloud.controller;

import com.erhui.springcloud.pojo.Dept;
import com.erhui.springcloud.service.DeptClientService;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud.config
 * @ClassName: DeptConsumerController
 * @Description:
 * @date 2020/3/13 14:38
 */
@RestController
public class DeptConsumerController {

    //注入springcloud-api中的接口
    @Autowired
    private DeptClientService deptClientService = null;

    @PostMapping("/consumer/dept/add")
    public boolean add(Dept dept) {
        return this.deptClientService.addDept(dept);
    }

    @GetMapping("/consumer/dept/get/{id}")
    public Dept getDeptById(@PathVariable("id") Long id) {
        return this.deptClientService.queryById(id);
    }

    @GetMapping("/consumer/dept/list")
    public List<Dept> list() {
        return this.deptClientService.queryAll();
    }

}
