package com.erhui.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud.config
 * @ClassName: ConfigBean
 * @Description:
 * @date 2020/3/13 14:56
 */
@Configuration //spring里的配置的注解  相当于 spring   applicationContext.xml
public class ConfigBean {

    //配置负载均衡实现RestTemplate
    //IRule 接口
    //RoundRobinRule 轮询
    //RandomRule     随机
    //AvailabilityFilteringRule  会先过滤掉,跳闸,访问故障的服务~对剩下的进行轮询
    //RetryRule   会先按照轮询获取服务,如果服务获取失败,则会在指定的时间内进行,重试
    @LoadBalanced
    //通过RestTemplate去请求provider的信息
    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
