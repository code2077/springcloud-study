package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan("com.erhui.springcloud")
@SpringBootApplication
@EnableEurekaClient
//告诉框架扫描所有使用注解@FeignClient定义的feign客户端
@EnableFeignClients(basePackages = {"com.erhui.springcloud"})

public class FeignDeptConsumer_80 {
    public static void main(String[] args) {
        SpringApplication.run(FeignDeptConsumer_80.class,args);
    }
}
