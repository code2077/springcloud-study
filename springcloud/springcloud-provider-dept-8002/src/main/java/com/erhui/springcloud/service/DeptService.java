package com.erhui.springcloud.service;

import com.erhui.springcloud.pojo.Dept;

import java.util.List;

public interface DeptService {
    //增加部门
    boolean addDept(Dept dept);
    //根据id查询部门
    Dept queryById(Long id);
    //查询所有
    List<Dept> queryAll();
}
