package com.erhui.springcloud.dao;

import com.erhui.springcloud.pojo.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface DeptDao {
    //增加部门
    boolean addDept(Dept dept);
    //根据id查询部门
    Dept queryById(Long id);
    //查询所有
    List<Dept> queryAll();
}
