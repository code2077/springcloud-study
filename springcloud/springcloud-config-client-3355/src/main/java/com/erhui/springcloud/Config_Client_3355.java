package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: Config_Client_3355
 * @Description:
 * @date 2020/3/18 21:21
 */
@SpringBootApplication
public class Config_Client_3355 {
    public static void main(String[] args) {
        SpringApplication.run(Config_Client_3355.class,args);
    }
}
