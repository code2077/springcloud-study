package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: DeptProvider_8001
 * @Description:
 * @date 2020/3/13 14:07
 */
//启动类
@SpringBootApplication
//Eureka是C-S架构,EnableEurekaClient则是开启了C,即客户端;服务启动后自动注册到Eureka
@EnableEurekaClient
@EnableDiscoveryClient//服务发现~
public class DeptProvider_8003 {
    public static void main(String[] args) {
        SpringApplication.run(DeptProvider_8003.class,args);
    }
}
