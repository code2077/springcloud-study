package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: Config_Server_3344
 * @Description:
 * @date 2020/3/18 11:35
 */
@SpringBootApplication
@EnableConfigServer //开启config服务
public class Config_Server_3344 {
    public static void main(String[] args) {
        SpringApplication.run(Config_Server_3344.class,args);
    }
}
