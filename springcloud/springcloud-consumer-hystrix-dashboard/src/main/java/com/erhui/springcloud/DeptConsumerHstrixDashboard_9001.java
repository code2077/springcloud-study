package com.erhui.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud
 * @ClassName: DdepHstrixDashboard
 * @Description:
 * @date 2020/3/17 16:31
 */
@SpringBootApplication
@EnableHystrixDashboard //开启
public class DeptConsumerHstrixDashboard_9001 {
    public static void main(String[] args) {
        SpringApplication.run(DeptConsumerHstrixDashboard_9001.class,args);
    }
}
