package com.erhui.springcloud.controller;

import com.erhui.springcloud.pojo.Dept;
import com.erhui.springcloud.service.DeptService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class DeptController {

    @Autowired
    private DeptService deptService;

    @GetMapping("/dept/get/{id}")
    @HystrixCommand(fallbackMethod = "hystrixGet")//有异常之后走hystrix指定的hystrixGet方法
    public Dept get(@PathVariable("id") Long id) {
        Dept dept = deptService.queryById(id);
        //如果dept为空
        if (dept == null) {
            throw new RuntimeException("id=>" + id + ",不存在该用户,或者信息无法找到");
        }

        return dept;
    }


    //备选方案
    public Dept hystrixGet(@PathVariable("id") Long id) {
        return new Dept().setDeptno(id)
                .setDname("id=>" + id + ",没有对应的信息,null---@Hystrix")
                .setDb_source("no this dataBase in MySQL");
    }

}
