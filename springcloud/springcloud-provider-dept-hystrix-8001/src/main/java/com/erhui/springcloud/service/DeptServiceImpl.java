package com.erhui.springcloud.service;

import com.erhui.springcloud.dao.DeptDao;
import com.erhui.springcloud.pojo.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud.service
 * @ClassName: DeptServiceImpl
 * @Description:
 * @date 2020/3/13 12:33
 */
@Service
public class DeptServiceImpl implements DeptService {

    @Autowired
    private DeptDao deptDao;

    @Override
    public boolean addDept(Dept dept) {
        return deptDao.addDept(dept);
    }

    @Override
    public Dept queryById(Long id) {
        return deptDao.queryById(id);
    }

    @Override
    public List<Dept> queryAll() {
        return deptDao.queryAll();
    }
}
