package com.erhui.springcloud.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 二灰
 * @PackageName: com.erhui.springcloud.pojo
 * @ClassName: Dept
 * @Description:
 * @date 2020/3/13 1:50
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Dept implements Serializable {//Dept 实体类  orm  类表关系映射

    private Long deptno;
    private String dname;
    private String db_source;

    public Dept(String dname) {
        this.dname = dname;
    }
}
