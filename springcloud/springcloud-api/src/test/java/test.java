import com.erhui.springcloud.pojo.Dept;

/**
 * @author 二灰
 * @PackageName: PACKAGE_NAME
 * @ClassName: test
 * @Description:
 * @date 2020/3/16 21:20
 */
public class test {
    public static void main(String[] args) {
        Dept dept = new Dept();
        dept.setDeptno(1L)
                .setDname("er")
                .setDb_source("ddd");
    }
}
